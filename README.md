# FVALDEZLAW

###### A Custom PochoLabs WordPress Theme

### Table of Contents

1. Theme Customization options
2. Want a new Feature?
3. Want a Custom theme like this for yourself?
4. Want to use this theme?

***

This is a custom theme for [fvaldezlaw.com](http://fvaldezlaw.com)

Custom Theme Development by Celso Mireles at [PochoLabs](http://pocholabs.com).

## Theme Customization options

This theme comes with a cool option for a home page. You can make it look like this:

![home](https://dl.dropboxusercontent.com/u/16972085/fvaldez-screenshots/home.png)

It is important to note that a lot of the customization may require a bit of knowledge of HTML and CSS and the [Bootstrap Framework](http://getbootstrap.com/). For example, on the home page, a panel showing different ways for visitors to contact FVALDEZLAW are shown through a Bootstrap Nav-Panel. Knowing the right HTML structure and appropriate classes is necessary to replicate this.

You may also have to include custom CSS to make the Bootstrap components branded. If you require such customization, feel free to contact me at [celso@digitalstrategy.tips](mailto:celso@digitalstrategy.tips).
***
In order customize the theme, there are some things you have to do first:

-1. Create a page called home and set the template to 'Home Page'

![home-page-template](https://dl.dropboxusercontent.com/u/16972085/fvaldez-screenshots/home-page-template.png)

-2. Set the front page to a static page. It is best to first create a page called 'Home' and select that page.

![set-static-page](https://dl.dropboxusercontent.com/u/16972085/fvaldez-screenshots/customizer-set-front-page.png)

-3. Now you have to add widgets with content. From the default 'Customizer' screen navigate to 'Widgets > Custom Home Page Header > Text':

![custom-home-page-header](https://dl.dropboxusercontent.com/u/16972085/fvaldez-screenshots/customizer-custom-header.png)

-4. Another customization option for the home page header section exists in 'Home Page > Header'. If you check "Use Image in Home Header", then the theme will look for an image. If not selected, it will just display Color Fallback.

![custom-header-image](https://dl.dropboxusercontent.com/u/16972085/fvaldez-screenshots/customizer-call-to-action.png)

-5. There is also an optional 'Call to Action' section where you can fully customize the content there using the text widget in the 'Call to Action' section. To get there from the default 'Customizer' view, navigate to 'Home Page > Call to Action > Text'. In order to show the section, you must select "Show Call to Action Section" and then select/upload an image. If no image is selected, then the Color Fallback will be used, which you can customize as well below:

![call-to-action](https://dl.dropboxusercontent.com/u/16972085/fvaldez-screenshots/customizer-call-to-action.png)

-6. You can also customize the footer text at the very bottom. In the theme customizer, navigate to 'Home Page > Footer'. Here you will be able to edit the text. You also have the option to include an parallax backgroung image, like in the rest of the home page.

![footer-text](https://dl.dropboxusercontent.com/u/16972085/fvaldez-screenshots/customizer-footer-text.png)

-7. You may have noticed that there are more section in the footer. These can be updated in the 'Widgets' section of the customizer. Each footer section can be customized individually.

![footer-widgets](https://dl.dropboxusercontent.com/u/16972085/fvaldez-screenshots/customizer-widgets.png)

### Enabling Blog Posts in Home Page

Go to the "Customizer" section of the WordPress backend.

![blog 01](https://dl.dropboxusercontent.com/u/16972085/fvaldez-screenshots/blog-01.png)

Go to the Blog section

![blog 02](https://dl.dropboxusercontent.com/u/16972085/fvaldez-screenshots/blog-02.png)

Click the Checkbox to enable showing a blog feed in the home page. You can also change the default value of blog posts to show in the home page.

![blog 03](https://dl.dropboxusercontent.com/u/16972085/fvaldez-screenshots/blog-03.png)


***

## Want a new Feature?

If there is a feature you would like to see, you can request it in this repository's Issue Tracker:

[https://bitbucket.org/pocholabs/fvaldezlaw-theme/issues](https://bitbucket.org/pocholabs/fvaldezlaw-theme/issues)

Per our support agreement, I will spend the appropriate amount of time to address the issues or feature requests.

## Want a Custom theme like this for yourself?

If you are not with FVALDEZLAW but like my work, feel free to reach out to me at [celso@digitalstrategy.tips](mailto:celso@digitalstrategy.tips) to start a discussion on how you can get a custom theme with personalized support. You can view my portfolio at [PochoLabs.com](http://pocholabs.com).

## Want to use this theme?

If you like this theme and want to use it, this theme IS licensed under the [GPL license](https://www.tldrlegal.com/l/gpl-3.0). I believe the true value lies in the support and customization I offer.

If this is useful to you, feel free to buy me a coffee so I can make more Open Source themes:

[https://cash.me/$celsom3](https://cash.me/$celsom3)
