<?php
// Get featured image to use as thumbnail
$thumbnail_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

// If no featured image, use this one
if (!$thumbnail_url):
	$thumbnail_url = "http://fvaldezlaw.com/wp-content/uploads/2016/06/no-preview.jpg";
endif;
?>

<div class="panel">
<div class="row">
<div class="col-lg-12">
<div class="row">

	<div class="col-sm-4">
		<div class="thumbnail-link">
			<a href="<?php the_permalink(); ?>">
				<img src="<?php echo $thumbnail_url; ?>" class="img-responsive feed-thumbnail" />
			</a>
		</div>
	</div>

	<div class="col-sm-8">
		<h2 class="feed-heading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<p class="feed-excerpt"><?php the_excerpt(); ?></p>
		<a href="<?php the_permalink(); ?>" class="feed-read-more">Read More...</a>
	</div>

</div>
</div>
</div>

</div>
<hr />
