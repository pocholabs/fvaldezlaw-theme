<?php
/**
 * _tk functions and definitions
 *
 * @package _tk
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 750; /* pixels */

if ( ! function_exists( '_tk_setup' ) ) :
/**
 * Set up theme defaults and register support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function _tk_setup() {
	global $cap, $content_width;



	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	/**
	 * Add default posts and comments RSS feed links to head
	*/
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	*/
	add_theme_support( 'post-thumbnails' );

	/**
	 * Enable support for Post Formats
	*/
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

	/**
	 * Setup the WordPress core custom background feature.
	*/
	// add_theme_support( 'custom-background', apply_filters( '_tk_custom_background_args', array(
	// 	'default-color' => 'ffffff',
	// 	'default-image' => '',
	// ) ) );

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on _tk, use a find and replace
	 * to change '_tk' to the name of your theme in all the template files
	*/
	load_theme_textdomain( '_tk', get_template_directory() . '/languages' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	*/
	register_nav_menus( array(
		'primary'  => __( 'Header bottom menu', '_tk' ),
	) );

}
endif; // _tk_setup
add_action( 'after_setup_theme', '_tk_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function _tk_widgets_init() {
	// register_sidebar( array(
	// 	'name'          => __( 'Sidebar', '_tk' ),
	// 	'id'            => 'sidebar-1',
	// 	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	// 	'after_widget'  => '</aside>',
	// 	'before_title'  => '<h3 class="widget-title">',
	// 	'after_title'   => '</h3>',
	// ) );

	// Custom Home Header widget
	register_sidebar( array(
	'name' => 'Custom Home Page Header',
	'id' => 'custom-home-page-header',
	'description' => 'Appears in the theme\'s custom Home Page header',
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => '',
	'after_title' => '',
	) );

	// Default Header Sidebar
	register_sidebar( array(
	'name' => 'Default Header Sidebar',
	'id' => 'default-header-sidebar',
	'description' => 'Appears by logo in page sidebar. (Except for custom home page)',
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => '',
	'after_title' => '',
	) );

	// Default Header Sidebar
	register_sidebar( array(
	'name' => 'Call to Action',
	'id' => 'call-to-action',
	'description' => 'Appears in Call to Action section on home page (if enabled in options)',
	'before_widget' => '<div class="content"><div class="container wow animated fadeInUp c2a">',
	'after_widget' => '</div></div>',
	'before_title' => '',
	'after_title' => '',
	) );


	// Footer widgets

	register_sidebar( array(
	'name' => 'Footer Sidebar 1',
	'id' => 'footer-sidebar-1',
	'description' => 'Appears in the footer area',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => '</aside>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => 'Footer Sidebar 2',
	'id' => 'footer-sidebar-2',
	'description' => 'Appears in the footer area',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => '</aside>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => 'Footer Sidebar 3',
	'id' => 'footer-sidebar-3',
	'description' => 'Appears in the footer area',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => '</aside>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );




}
add_action( 'widgets_init', '_tk_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function _tk_scripts() {

	/**
*	Use latest jQuery release
*/
if( !is_admin() ){
	wp_deregister_script('jquery');
	wp_register_script('jquery', ("http://code.jquery.com/jquery-latest.min.js"), false, '');
	wp_enqueue_script('jquery');
}

	// Import the necessary TK Bootstrap WP CSS additions
	wp_enqueue_style( '_tk-bootstrap-wp', get_template_directory_uri() . '/includes/css/bootstrap-wp.css' );

	// load bootstrap css
	wp_enqueue_style( '_tk-bootstrap', get_template_directory_uri() . '/src/css/bootstrap.css' );

	// load Font Awesome css
	wp_enqueue_style( '_tk-font-awesome', get_template_directory_uri() . '/src/css/font-awesome.css', false, '4.1.0' );

	wp_enqueue_style( '_tk-animate', get_template_directory_uri() . '/src/css/animate.min.css', false, '4.1.0' );

	wp_enqueue_style( '_tk-owl', get_template_directory_uri() . '/src/css/owl.carousel.css', false, '4.1.0' );

	wp_enqueue_style( '_tk-owl-theme', get_template_directory_uri() . '/src/css/owl.theme.css', false, '4.1.0' );

	wp_enqueue_style( '_tk-owl-transitions', get_template_directory_uri() . '/src/css/owl.transitions.css', false, '4.1.0' );

	// load src css
	wp_enqueue_style( '_wireframe-css', get_template_directory_uri() . '/src/main.css' );

	// load _tk styles
	wp_enqueue_style( '_tk-style', get_stylesheet_uri() );

	// load bootstrap js
	wp_enqueue_script('_tk-bootstrapjs', get_template_directory_uri().'/includes/resources/bootstrap/js/bootstrap.min.js', array('jquery') );

	// load bootstrap wp js
	wp_enqueue_script( '_tk-bootstrapwp', get_template_directory_uri() . '/includes/js/bootstrap-wp.js', array('jquery') );

	wp_enqueue_script( '_tk-skip-link-focus-fix', get_template_directory_uri() . '/includes/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( '_tk-keyboard-image-navigation', get_template_directory_uri() . '/includes/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
	}

	// Custom Theme Script from src
	wp_enqueue_script('_wf-animated-header', get_template_directory_uri() . '/src/js/cbpAnimatedHeader.js');

	wp_enqueue_script('_wf-anystretch', get_template_directory_uri() . '/src/js/jquery.anystretch.min.js');

	wp_enqueue_script('_wf-lettering', get_template_directory_uri() . '/src/js/jquery.lettering.js');

	wp_enqueue_script('_wf-textillate', get_template_directory_uri() . '/src/js/jquery.textillate.js');

	wp_enqueue_script('_wf-owl', get_template_directory_uri() . '/src/js/owl.carousel.min.js');

	wp_enqueue_script('_wf-lettering', get_template_directory_uri() . '/src/js/jquery.lettering.js');

	wp_enqueue_script('_wf-parallax', get_template_directory_uri() . '/src/js/parallax.min.js');

	wp_enqueue_script('_wf-twitter-fetcher', get_template_directory_uri() . '/src/js/twitterFetcher_min.js');

	wp_enqueue_script('_wf-wow', get_template_directory_uri() . '/src/js/wow.min.js');

	wp_enqueue_script('_wf-main', get_template_directory_uri() . '/src/js/main.js');


}
add_action( 'wp_enqueue_scripts', '_tk_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/includes/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/includes/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/includes/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/includes/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/includes/jetpack.php';

/**
 * Load custom WordPress nav walker.
 */
require get_template_directory() . '/includes/bootstrap-wp-navwalker.php';

function remove_cssjs_ver( $src ) {
    if( strpos( $src, '?ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );
