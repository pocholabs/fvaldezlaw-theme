<?php
/**
 * Template Name: News Page
 *
 * This is the template that displays the theme's
 * custom Home Page.
 *
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package _tk
 */

get_header(); ?>

	<?php //if ( have_posts() ) : ?>

		<?php /* Start the Loop */

		$wp_query = new WP_Query( array(
			'post_type' => 'post',
			'posts_per_page' => '3',
			'category_name' => 'news'
			) );

		if ($wp_query->have_posts() ):

			while ( $wp_query->have_posts() ) : $wp_query->the_post();

				get_template_part( 'content', 'news' );

			endwhile; ?>


		<?php _tk_content_nav( 'nav-below' ); ?>
		<?php //wp_pagenavi(); ?>

	<?php else : ?>

		<?php get_template_part( 'no-results', 'index' ); ?>

	<?php endif; ?>



<?php //get_sidebar(); ?>
<?php get_footer(); ?>
