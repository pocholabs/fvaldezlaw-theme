<?php
/**
 * Template Name: Home Page
 *
 * This is the template that displays the theme's
 * custom Home Page.
 *
 * @package _tk
 */

get_header('home'); ?>

<?php // This initial loop gets the visible services section ?>

<div class="section white">
	<div class="content">
		<div id="solutions" class="container wow animated fadeInUp">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="text-center">Solutions For:</h2>
				</div>
			</div>
			<div class="row">

				<?php $loop = new WP_Query( array( 'post_type' => 'resource', 'posts_per_page' => 8 ) ); ?>
				<?php
					while ( $loop->have_posts() ) : $loop->the_post();

						$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
						$post_id = $post->ID;
				?>

					<div class="col-sm-6 col-md-3">
						<a type="button" data-toggle="modal" data-target="#<?php echo $post_id; ?>"><div style="visibility: visible; background: url('<?php echo $url; ?>');background-position:center;background-size:cover;background-repeat:no-repeat;" class="service wow">
							<div class="service-box">
								<h3><?php the_title() ?></h3>
							</div>
						</div>
						</a>
					</div>

				<?php endwhile; wp_reset_query(); ?>

			</div>
		</div>
	</div>
</div>




<?php
	$show_cta = get_theme_mod( 'show_cta' );



	if($show_cta === 1){
		$cta_image_upload = get_theme_mod( 'cta-image-upload' );
		$cta_color_fallback = '';
		$cta_fallback_style = '';

		if(!$cta_image_upload){
			$cta_color_fallback = get_theme_mod( 'cta_color_fallback' );
			$cta_fallback_style = 'background:' . $cta_color_fallback . ';';
		}
		?>

		<div data-image-src="<?php echo $cta_image_upload; ?>" data-parallax="scroll" class="parallax-window">
		  <div class="section overlay-dark" style="<?php echo $cta_fallback_style; ?>">
		  	<div class="content">
		  	  <div class="container wow animated fadeInUp c2a">
						<?php
						if(is_active_sidebar('call-to-action')){
						dynamic_sidebar('call-to-action');
						}
						?>

		  	  </div>
		  	</div>
		  </div>
		</div>

		<?php
	}
 ?>

 <?php //This is where we run a loop on latest blog posts

	$show_blog = get_theme_mod( 'show_blog' );

	if($show_blog === 1) {



		$blog_entry_number = get_theme_mod('blog_entry_number');

		if ( !is_int( $blog_entry_number ) ){
			$blog_entry_number = intval( $blog_entry_number );
		}

 ?>

 <div class="section white">
 	<div class="container">
 		<div class="row">
			<?php

				$wp_query = new WP_Query( array(
					'post_type' => 'post',
					'posts_per_page' => $blog_entry_number,
					'category_name' => 'news'
				) );

				if ($wp_query->have_posts() ):

 				while ( $wp_query->have_posts() ) : $wp_query->the_post();

 					$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
 					$post_id = $post->ID;
 			?>

 				<div class="col-md-12">
 					<div class="panel">
 									<?php
 										/* Include the Post-Format-specific template for the content.
 										 * If you want to overload this in a child theme then include a file
 										 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
 										 */
 										get_template_part( 'content', 'news' );
 									?>
 					</div>
 				</div>

 			<?php endwhile;  ?>

		<?php else: ?>

			<?php get_template_part( 'no-results', 'index' ); ?>

		<?php endif;

		wp_reset_query(); ?>

 		</div>
 	</div>
 </div>

 <?php } ?>


<div class="section off-white">
  <div class="content">
    <div class="container wow animated fadeInUp">
      <h2>Latest Tweets</h2>
      <div id="twitter-fetch" class="simple-slider"></div>
    </div>
  </div>
</div>




<?php //Now we re run the loop to generate the modal bodies ?>


<?php $loop = new WP_Query( array( 'post_type' => 'resource', 'posts_per_page' => 8 ) ); ?>
<?php
	while ( $loop->have_posts() ) : $loop->the_post();

		$post_id = $post->ID;
?>


	<div id="<?php echo $post_id; ?>" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" data-dismiss="modal" class="close">&times;</button>
				</div>
				<div class="modal-body">
					<h2><?php the_title() ?></h2>
					<?php the_content() ?>
				</div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
				</div>
			</div>
		</div>
	</div>





<?php endwhile; wp_reset_query(); ?>



<?php get_footer(); ?>
