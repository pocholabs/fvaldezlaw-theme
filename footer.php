<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package _tk
 */
?>

<?php

 if(is_front_page()){
	 ?><?php
 }else{
	 ?></div><!-- close .main-content --><?php

 }

	$show_footer_parallax = get_theme_mod( 'show_footer_parallax' );
	$footer_image_upload = get_theme_mod( 'footer-image-upload' );
	$footer_color_fallback = '';
	$footer_fallback_style = '';

	if($show_footer_parallax !== 1) {
		$footer_color_fallback = get_theme_mod( 'footer_color_fallback' );
		$footer_fallback_style = 'background:' . $footer_color_fallback . ';';
	}

?>

<div class="parallax-window" data-image-src="http://s20.postimg.org/tkxo2tqm5/5021872334_0d2b372091_o.jpg" data-parallax="scroll">
	<footer id="colophon" class="site-footer" role="contentinfo" style="<?php echo $footer_fallback_style; ?>">
	<?php // substitute the class "container-fluid" below if you want a wider content area ?>
		<div class="container">

			<div class="row">
			  <div class="col-sm-4">
					<?php
						if(is_active_sidebar('footer-sidebar-1')){
						dynamic_sidebar('footer-sidebar-1');
						}
					?>


			  </div>
			  <div class="col-sm-4">
					<?php
						if(is_active_sidebar('footer-sidebar-2')){
						dynamic_sidebar('footer-sidebar-2');
						}
					?>

			  </div>
			  <div class="col-sm-4">
					<?php
					if(is_active_sidebar('footer-sidebar-3')){
					dynamic_sidebar('footer-sidebar-3');
					}
					?>

			  </div>
			</div>

			<div class="row">
				<div class="site-footer-inner col-sm-12 text-center">

					<div class="site-info">
						<?php do_action( '_tk_credits' ); ?>
						<a href="http://wordpress.org/" title="<?php esc_attr_e( 'A Semantic Personal Publishing Platform', '_tk' ); ?>" rel="generator"><?php printf( __( 'Proudly powered by %s', '_tk' ), 'WordPress' ); ?></a>
						<span class="sep"> | </span>
	                    <?php echo get_theme_mod('custom_copyright', '<a class="credits" href="http://pocholabs.com/" target="_blank" title="Themes and Plugins developed by PochoLabs">Themes and Plugins developed by PochoLabs. </a> © 2016 FValdezLaw PC'); ?>
					</div><!-- close .site-info -->

				</div>
			</div>
		</div><!-- close .container -->
	</footer><!-- close #colophon -->
</div><!-- close .parallax-window -->

<?php wp_footer(); ?>

</body>
</html>
