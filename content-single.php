<?php
/**
 * @package _tk
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if (has_post_thumbnail() && !empty(wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' )[0]) ){
		$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
		// style="background:url('<php echo $large_image_url[0]; >');background-position: center;background-repeat: no-repeat;background-size: cover;"
		// style="background:rgba(7,35,49,0.9);height:200px;"
		// data-parallax="scroll" class="parallax-window row" data-img-src="http://s20.postimg.org/g7pxmgwe5/16248851160_87d8572f80_z.jpg"
		?>
		<div class="row" style="margin-top:-50px;margin-bottom:20px;background:url('<?php echo $large_image_url[0]; ?>');background-position: top;background-repeat: no-repeat;background-size: cover;">
			<div style="background:rgba(7,35,49,0.85);height:200px;">
				<header>
					<h1 class="page-title text-center" style="color:#fff;padding: 80px 100px;"><?php the_title(); ?></h1>
				</header><!-- .entry-header -->
			</div>
		</div>
		<?php
	}else{
		?>
		<header>
			<h1 class="page-title"><?php the_title(); ?></h1>
		</header><!-- .entry-header -->
		<?php
	} ?>


	<div class="row">
		<div class="col-md-12">
			<div class="entry-content">
				<div class="entry-content-thumbnail">
					<?php // the_post_thumbnail(); ?>
				</div>
				<?php the_content(); ?>
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', '_tk' ),
						'after'  => '</div>',
					) );
				?>
			</div><!-- .entry-content -->
		</div>
	</div>


	<footer class="entry-meta panel panel-default">
		<div class="entry-meta">
			<?php _tk_posted_on(); ?>
		</div><!-- .entry-meta -->
		<hr/>
		<?php
			/* translators: used between list items, there is a space after the comma */
			$category_list = get_the_category_list( __( ', ', '_tk' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', '_tk' ) );

			if ( ! _tk_categorized_blog() ) {
				// This blog only has 1 category so we just need to worry about tags in the meta text
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', '_tk' );
				} else {
					$meta_text = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', '_tk' );
				}

			} else {
				// But this blog has loads of categories so we should probably display them here
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', '_tk' );
				} else {
					$meta_text = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', '_tk' );
				}

			} // end check for categories on this blog

			printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink(),
				the_title_attribute( 'echo=0' )
			);
		?>

		<?php edit_post_link( __( 'Edit', '_tk' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-## -->
