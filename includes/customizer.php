<?php
/**
 * _tk Theme Customizer
 *
 * @package _tk
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function _tk_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	$wp_customize->add_panel( 'home-panel', array(
	  'title' => __( 'Home Page' ),
	  'description' => '<p>Make sure you set your front page to static page, and that page is using the Home Page template</p>', // Include html tags such as <p>.
	  'priority' => 160, // Mixed with top-level-section hierarchy.
	) );

	//=====================================================
	//=====================================================
	//
	// Add 'Home Page' section to customizer
	//
	//=====================================================
	//=====================================================

	$wp_customize->add_section( 'home-page' , array(
	  'title' => __( 'Header', 'FVcustom' ),
	  'priority' => 190,
		'panel' => 'home-panel',
	) );

	// Option for whether to show an image in the header
	$wp_customize->add_setting( 'show_header_1', array(
		'sanitize_callback' => 'sanitize_checkbox',
	));

	$wp_customize->add_control(
    'show_header_1',
    array(
        'type' => 'checkbox',
        'label' => 'Use image in Home header',
        'section' => 'home-page',
    )
	);


	// Home Page Header image
	$wp_customize->add_setting( 'home-hero-upload' );

	$wp_customize->add_control(
	    new WP_Customize_Image_Control(
	        $wp_customize,
	        'home-hero-upload',
	        array(
	            'label' => 'Choose an Image for Home header section',
	            'section' => 'home-page',
	            'settings' => 'home-hero-upload'
	        )
	    )
	);

	// Color fallback in case image in header is not shown
	$wp_customize->add_setting(
    'hero_color_fallback',
    array(
        'default' => '#072331',
        'sanitize_callback' => 'sanitize_hex_color',
    )
	);

	$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'hero_color_fallback',
        array(
            'label' => 'Color Fallback (In case Image is not chosen)',
            'section' => 'home-page',
            'settings' => 'hero_color_fallback',
        )
    )
	);






	//=====================================================
	//=====================================================
	//
	// Add 'Call to Action' section to customizer
	//
	//=====================================================
	//=====================================================

	$wp_customize->add_section( 'cta-section' , array(
	  'title' => __( 'Call to Action', 'FVcustom' ),
	  'priority' => 200,
		'panel' => 'home-panel',
	) );

	// Option for whether to show an image in the header
	$wp_customize->add_setting( 'show_cta', array(
		'sanitize_callback' => 'sanitize_checkbox',
	));

	$wp_customize->add_control(
    'show_cta',
    array(
        'type' => 'checkbox',
        'label' => 'Show Call to Action section?',
        'section' => 'cta-section',
    )
	);

	// Call to Action Parallax image
	$wp_customize->add_setting( 'cta-image-upload' );

	$wp_customize->add_control(
	    new WP_Customize_Image_Control(
	        $wp_customize,
	        'cta-image-upload',
	        array(
	            'label' => 'Choose an Image for Call to Action',
	            'section' => 'cta-section',
	            'settings' => 'cta-image-upload'
	        )
	    )
	);

	// Color fallback in case image in header is not shown
	$wp_customize->add_setting(
    'cta_color_fallback',
    array(
        'default' => '#072331',
        'sanitize_callback' => 'sanitize_hex_color',
    )
	);

	$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'cta_color_fallback',
        array(
            'label' => 'Color Fallback',
            'section' => 'cta-section',
            'settings' => 'cta_color_fallback',
        )
    )
	);


	//=====================================================
	//=====================================================
	//
	// Add 'Blog' section to customizer
	//
	//=====================================================
	//=====================================================

	// Adding the 'blog' section to the home panel in customizer
	$wp_customize->add_section( 'blog-section' , array(
		'title' => __( 'Blog', 'FVcustom' ),
		'priority' => 201,
		'panel' => 'home-panel',
	) );

	// Option for whether to show the blog section in the home page

	$wp_customize->add_setting( 'show_blog', array(
		'sanitize_callback' => 'sanitize_checkbox',
	));

	$wp_customize->add_control(
		'show_blog',
		array(
				'type' => 'checkbox',
				'label' => 'Show Blog Feed in Home Page?',
				'section' => 'blog-section',
		)
	);


	// Option for how many blog posts to show

$wp_customize->add_setting( 'blog_entry_number', array(
	'default' => '4'
) );

$wp_customize->add_control( 'blog_entry_number', array(
    'type' => 'text',

    'section' => 'blog-section',
    'label' => 'Number of Posts to Show:',
    'description' => 'How many should show up in the Home Page Feed?'
) );


	//=====================================================
	//=====================================================
	//
	// Footer section
	//
	//=====================================================
	//=====================================================



	$wp_customize->add_section( 'footer-section' , array(
	  'title' => __( 'Footer', 'FVcustom' ),
	  'priority' => 202,
		'panel' => 'home-panel',
	) );

	// Option for whether to show an image in the header
	$wp_customize->add_setting( 'show_footer_parallax', array(
		'sanitize_callback' => 'sanitize_checkbox',
	));

	$wp_customize->add_control(
		'show_footer_parallax',
		array(
				'type' => 'checkbox',
				'label' => 'Use parallax background image in Footer',
				'section' => 'footer-section',
		)
	);

	$wp_customize->add_setting( 'custom_copyright', array(
	  'default' => '<a class="credits" href="http://pocholabs.com/" target="_blank" title="Themes and Plugins developed by PochoLabs" alt="Themes and Plugins developed by Celso Mireles at PochoLabs">Themes and Plugins developed by PochoLabs. </a>',
	  'sanitize_callback' => 'sanitize_text',
	) );

	$wp_customize->add_control( 'custom_copyright', array(
	  'label' => __( 'Copyright Text' ),
	  'type' => 'textarea',
	  'section' => 'footer-section',
	) );

	// Footer Parallax image
	$wp_customize->add_setting( 'footer-image-upload' );

	$wp_customize->add_control(
	    new WP_Customize_Image_Control(
	        $wp_customize,
	        'footer-image-upload',
	        array(
	            'label' => 'Choose an Image for Footer',
	            'section' => 'footer-section',
	            'settings' => 'footer-image-upload'
	        )
	    )
	);

	// Color fallback in case image in footer image is not shown
	$wp_customize->add_setting(
    'footer_color_fallback',
    array(
        'default' => '#072331',
        'sanitize_callback' => 'sanitize_hex_color',
    )
	);

	$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'footer_color_fallback',
        array(
            'label' => 'Color Fallback',
            'section' => 'footer-section',
            'settings' => 'footer_color_fallback',
        )
    )
	);









}
add_action( 'customize_register', '_tk_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function _tk_customize_preview_js() {
	wp_enqueue_script( '_tk_customizer', get_template_directory_uri() . '/includes/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', '_tk_customize_preview_js' );

function sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}

function sanitize_checkbox( $input ) {
    if ( $input == 1 ) {
        return 1;
    } else {
        return '';
    }
}
