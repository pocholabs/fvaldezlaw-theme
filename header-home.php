<?php
/**
 * The Header for our theme's Home Page
 *
 * Displays all of the <head> section
 *
 * @package _tk
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php do_action( 'before' );


	$show_home_hero = get_theme_mod('show_header_1');
	$hero_color_fallback = get_theme_mod( 'hero_color_fallback' );
	$home_hero_image = '';
	$parallax_class = '';
	$hero_fallback_style = '';
	$feature_gradient = '';

	if($show_home_hero === 1){
		$home_hero_image = get_theme_mod('home-hero-upload');
		$parallax_class = 'parallax-window';
		$feature_gradient = 'feature-gradient';

	}else{
		$hero_fallback_style = 'style="background:' . $hero_color_fallback . ';"';
	}


	?>

		<!-- http://s20.postimg.org/ayzi2c3ct/hero.jpg -->
		<div class="feature-bg <?php echo $parallax_class; ?>" data-image-src="<?php echo $home_hero_image; ?>" data-parallax="scroll">

			<div class="container-fluid <?php echo $feature_gradient; ?>" <?php echo $hero_fallback_style; ?>">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<nav class="site-navigation navbar navbar-default navbar-fixed-top" role="navigation">
							<?php // substitute the class "container-fluid" below if you want a wider content area ?>
								<div class="container">
									<div class="row">
										<div id="masthead" class="col-md-5 col-sm-12 col-xs-7">
											<div class="logo-area">
												<?php $logoUrl = get_template_directory_uri() . '/src/img/logo-fvaldez.png'; ?>
												<img src="<?php echo $logoUrl; ?>" alt="" class="img-responsive hidden-xs home">
												<p class="text-center header-logo-text"><span class="t-fade">F</span><span class="t-white">VALDEZ</span><span class="t-fade">LAW</span><span class="t-white hidden-xs"> PC</span></p>
											</div>
										</div> <!-- end logo section -->


										<div class="col-md-7 col-sm-12 col-xs-5">

												<div class="navbar-header">
													<!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
													<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
														<span class="sr-only"><?php _e('Toggle navigation','_tk') ?> </span>
														<span class="icon-bar"></span>
														<span class="icon-bar"></span>
														<span class="icon-bar"></span>
													</button>

													<!-- Your site title as branding in the menu -->

												</div>
											</div>

											<div class="collapse navbar-collapse navbar-right">

												<!-- The WordPress Menu goes here -->
												<?php wp_nav_menu(
													array(
														'theme_location' 	=> 'primary',
														'depth'             => 2,
														'container'         => '',
														'container_class'   => 'collapse navbar-collapse',
														'menu_class' 		=> 'nav navbar-nav',
														'fallback_cb' 		=> 'wp_bootstrap_navwalker::fallback',
														'menu_id'			=> 'main-menu',
														'walker' 			=> new wp_bootstrap_navwalker()
													)
												); ?>


										</div>
									</div>
								</div><!-- .container -->
							</nav><!-- .site-navigation -->
						</div>
					</div>
				</div>


				<?php
				if(is_active_sidebar('custom-home-page-header')){
				dynamic_sidebar('custom-home-page-header');
				}
				?>


			</div>



		</div>
